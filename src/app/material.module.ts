import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  MatButtonModule, MatFormFieldModule, MatSelectModule, MatListModule, MatInputModule} from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatFormFieldModule, MatSelectModule, MatListModule, MatInputModule],
  exports: [MatButtonModule, MatFormFieldModule, MatSelectModule, MatListModule, MatInputModule]
})
export class MaterialAppModule { }
