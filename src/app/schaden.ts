export class Schaden {

  public id: number;
  public hersteller: string;
  public art: string;
  public produktbezeichnung: string;
  public fehlerbeschreibung: string;
}
