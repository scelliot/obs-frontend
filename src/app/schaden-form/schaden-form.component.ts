import { Component, OnInit } from '@angular/core';
import {Schaden} from '../schaden';
import {RestService} from '../rest.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-schaden-form',
  templateUrl: './schaden-form.component.html',
  styleUrls: ['./schaden-form.component.scss']
})
export class SchadenFormComponent implements OnInit {

  schaden = new Schaden();
  submitted = 'false';
  herstellerList = [];
  artList: string[] = [];

  constructor(private _restService: RestService, private http: HttpClient) { }

  ngOnInit() {
    this.artList = ['Mikrowelle', 'Herd'];
    this.schaden.produktbezeichnung = '';
    this.getHerstellerList();
    this.schaden.fehlerbeschreibung = '';
  }

  getHerstellerList() {
    this._restService.getHerstellerList()
      .subscribe(data => {
        console.log('erster Hersteller: ' + data[0].bezeichnung);
        for (let item of data) {
          this.herstellerList.push(item.bezeichnung);
        }
      });
  }

  onSubmit() {
    this.submitted = 'true';
    console.log('Hersteller: ' + this.schaden.hersteller);
    console.log('Art: ' + this.schaden.art);
    console.log('Produktbezeichnung: ' + this.schaden.produktbezeichnung);
    console.log('Fehlerbeschreibung: ' + this.schaden.fehlerbeschreibung);
  }
}
