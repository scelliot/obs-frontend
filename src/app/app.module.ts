import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialAppModule} from './material.module';
import { SchadenFormComponent } from './schaden-form/schaden-form.component';
import { FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RestService } from './rest.service';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { FragmentPolyfillModule } from './fragment-polyfill.module';

let routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'app'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    SchadenFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialAppModule,
    FormsModule,
    HttpClientModule,
    FragmentPolyfillModule.forRoot({
      smooth: true
    }),
    RouterModule.forRoot(
      routes,
      {
        // Tell the router to use the HashLocationStrategy.
        useHash: true,
        enableTracing: false
      }
    )
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
