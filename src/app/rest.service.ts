import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

interface HerstellerResponse {
  id: string;
  bezeichnung_schluessel: string;
  bezeichnung: string;
}

@Injectable()
export class RestService {

  constructor(private http: HttpClient ) { }

  getHerstellerList(): Observable<HerstellerResponse[]> {
    return this.http.get<HerstellerResponse[]>('http://localhost:8080/hersteller');
  }

}
